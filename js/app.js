// Arrow down functionality

$(document).ready(function(){

	var $cache = {
		bodyContainer : $('body'),
		jumboContainer : $('.jumbo-hp'),
		scrollDownBtn : $('.btn-scrolldown'),
		scrollTo : $('.scrollTo'),
		headerContainer : $('.header'),
		headerOffsetBottom : $('.header').height(),
		headerNavigation : $('.header-navigation'),
		responsiveMenu : $('#header-menu'),
		overlayContainer : $('#overlay'),
		homepageJumbo : $('.homepage .jumbo-block'),
		jobPopup : $('.jobDetailsLink'),
		contactMarker : $('.marker')
	}

	// Scroll Down functionality (OPT)
	$cache.scrollDownBtn.on('click', function(e){
		e.preventDefault();
		var scrollToPosition = $cache.scrollTo.offset().top - $cache.headerOffsetBottom;
		$('html, body').animate({
			scrollTop: scrollToPosition
		}, 1500);
	});


	// Animate count (OPT)
	var animateCount = function(toNumber, toId) {
		$({numberValue: 0}).animate({numberValue: toNumber}, {
			duration: 700,
			easing: 'linear',
			progress: function() {
				$(toId).text(Math.ceil(this.numberValue*100)/100 + "%");
			}
		});
	};

	// Job details popup
	$cache.jobPopup.on('click', function(e){
		var content = $(this).parent().find('.job-details').html();

		e.preventDefault();
		$cache.overlayContainer.fadeIn('fast', function(){
			$('.team-popup').html(content).fadeIn();
		});
	})

	// Transition (OPT)

	var animationFunc = function () {
		if ($(window).width() > 1023) {
			setTimeout(function(){
				$('.to-anim-1').addClass('visible animated fadeInLeft');
			},100);

			setTimeout(function(){
				$('.to-anim-2').addClass('visible animated fadeInUp');
			},1300);

			setTimeout(function(){
				$('.to-anim-3').addClass('visible animated fadeInUp');
			},2000);

			$('.anim-4').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 100
			});

			$('.anim-5').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 150
			});

			$('.anim-6').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 150
			});

			$('.contact-map').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeIn',
				offset: 300
			});

			$('.marker').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 100
			});

			$('.anim-7').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 100
			});

			$('.features-container h3').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 100
			});

			$('.increase-sub-bottom .i-arrow').addClass("hidden").viewportChecker({
				classToAdd: 'visible animated fadeInUp',
				offset: 100,
			});

			$('.increase-sub-bottom .text-big').addClass("hidden").viewportChecker({
				classToAdd: 'visible green animated fadeInUp',
				offset: 100,
			});


		}
	};
	animationFunc();

	$(window).resize(function(){
		animationFunc();
	})

	// Video autoplay(OPT)
	$(window).scroll(function() {
		$('video').each(function(){
			if ($(this).is(":in-viewport( 400 )")) {
				$(this)[0].play();
			} else {
				$(this)[0].pause();
			}
		});
	});


	// Contact markers (OPT)
	$cache.contactMarker.on('click', function(){
		if ($(window).width() > 767) {
			$('.marker-content').fadeOut('slow');
			$('.marker').css('z-index', 0);
			$(this).find('.marker-content').fadeIn().addClass('active');
			$(this).find('.marker-content').parent().css('z-index', 1);
		}

	})

	// Slider (OPT)
	var initialValue = 100; // initial slider value
	var sliderTooltip = function(event, ui) {
		var curValue = ui.value || initialValue; // current value (when sliding) or initial value (at start)
		var tooltip = '<div class="slider-tooltip"><div class="tooltip-inner">' + curValue + '</div><div class="tooltip-arrow"></div></div>';

		$('.ui-slider-handle').html(tooltip); //attach tooltip to the slider handle

	}

	if ($(window).width() < 1025) {
		$('.ui-slider-handle').draggable({
			axis: "x"
		});
	}

	$( "#slider" ).slider({
		value:200,
		min: 10,
		max: 1000,
		step: 5,
		create: sliderTooltip,
		slide: function( event, ui ) {
			var patientsValue = (ui.value * 0.99).toFixed(),
				revenueValue = (ui.value * 0.67).toFixed();

			$('.slider-tooltip .tooltip-inner').html(ui.value);
			$('#patientsCount').html(patientsValue);
			$('#revenueCount').html(revenueValue);
		}
	});
	$( "#amount" ).val( "$" + $( "#slider" ).slider( "value" ) );

	// TABS (OPT)
	$(".tab-heading li").click(function(e) {

		if (!$(this).hasClass('active')){
			if ($(this).hasClass('tab-1')){
				$('.mid-container-l').find('.active').addClass('animated fadeOutLeft');
				setTimeout(function(){
					$('.mid-container-l').find('.active').addClass('hide').removeClass('active animated fadeOutLeft fadeInLeft');
					$('.tab-slide-1').removeClass('hide').addClass('active animated fadeInLeft');
				},700);
				$('.tab-content').find('.active').fadeOut('fast', function(){
					$(this).removeClass('active').addClass('hide');
					$('.tab-content-1').fadeIn('slow', function(){
						$(this).removeClass('hide').addClass('active');
					})
				})

			} else if ($(this).hasClass('tab-2')) {
				$('.mid-container-l').find('.active').addClass('animated fadeOutLeft');
				setTimeout(function(){
					$('.mid-container-l').find('.active').addClass('hide').removeClass('active animated fadeOutLeft fadeInLeft');
					$('.tab-slide-2').removeClass('hide').addClass('active animated fadeInLeft');
				},700);
				$('.tab-content').find('.active').fadeOut('slow', function(){
					$(this).removeClass('active').addClass('hide');
					$('.tab-content-2').fadeIn('slow', function(){
						$(this).removeClass('hide').addClass('active');
					})
				})
			} else if ($(this).hasClass('tab-3')) {
				$('.mid-container-l').find('.active').addClass('animated fadeOutLeft');
				setTimeout(function(){
					$('.mid-container-l').find('.active').addClass('hide').removeClass('active animated fadeOutLeft fadeInLeft');
					$('.tab-slide-3').removeClass('hide').addClass('active animated fadeInLeft');
				},700);
				$('.tab-content').find('.active').fadeOut('slow', function(){
					$(this).removeClass('active').addClass('hide');
					$('.tab-content-3').fadeIn('slow', function(){
						$(this).removeClass('hide').addClass('active');
					})
				})
			}
		}

		$('.tab-heading').find('li').each(function(){
			$(this).removeClass('active');
		});

		$(this).addClass('active');

		if ($(this).hasClass('slider')) {
			return;
		}

		var whatTab = $(this).index(),
			howFar = 160;

		var detectHowFar = function() {
			howFar = $('.tab-1').width() * whatTab;
		}

		detectHowFar();

		$(window).on('resize', function(){
			detectHowFar();
		})


		$(".slider").css({
			left: howFar + "px"
		});
	});

	$('#andrewPopup').on('click', function(){
		if ($(window).width() > 767) {
			$('#overlay').fadeIn('slow', function(){
				$('#popup-andrew').fadeIn();
			});
		}

	});

	$('#pontusPopup').on('click', function(){
		if ($(window).width() > 767) {
			$('#overlay').fadeIn('slow', function(){
				$('#popup-pontus').fadeIn();
			});
		}
	});

	$('#tonyPopup').on('click', function(){
		if ($(window).width() > 767) {
			$('#overlay').fadeIn('slow', function(){
				$('#popup-tony').fadeIn();
			});
		}
	});

	$('#cristianPopup').on('click', function(){
		if ($(window).width() > 767) {
			$('#overlay').fadeIn('slow', function(){
				$('#popup-cristian').fadeIn();
			});
		}
	});

	$('#ramonPopup').on('click', function(){
		if ($(window).width() > 767) {
			$('#overlay').fadeIn('slow', function(){
				$('#popup-ramon').fadeIn();
			});
		}
	});


	// Fixed header (OPT)
	$(window).scroll(function(){
		var topPosition = $(window).scrollTop();
		if (topPosition > $cache.headerOffsetBottom) {
			$cache.headerContainer.addClass('fullcolor');
		} else {
			$cache.headerContainer.removeClass('fullcolor');
		}
	});

	// Header menu responsive (OPT)
	$cache.responsiveMenu.on('click', function(){
		$cache.headerNavigation.slideToggle();
		$cache.overlayContainer.toggle();
		$cache.bodyContainer.toggleClass('menu-open')
		if (!$cache.headerContainer.hasClass('fullcolor')){
			$cache.headerContainer.toggleClass('fullcolor');
		};
	});

	$cache.overlayContainer.on('click', function(){
		var windowWidth = $(window).width();
		$(this).hide();
		$('.team-popup').fadeOut();
		if ( windowWidth < 768 ) {
			$cache.headerNavigation.slideToggle();
			$cache.bodyContainer.removeClass('menu-open');
		}
		if ($cache.contactSuccess.length > -1) {
			$cache.contactSuccess.fadeOut();
		}
	})

	// Transition viewport
	$('#processAsses, #processDeliver').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeInLeft',
		offset: 100
	});

	$('#processTrust').addClass("hidden").viewportChecker({
		classToAdd: 'visible animated fadeInRight',
		offset: 100
	});

});